//
//  Trilogia.h
//  Personajes
//
//  Created by S209e19 on 28/08/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Trilogia : NSObject

@property (assign, nonatomic) NSUInteger weight;
@property (assign, nonatomic) NSUInteger age;

- (instancetype)initWithWeight:(NSUInteger)weight age:(NSUInteger)age;

+ (NSString *)giveMeMyTrilogyWithMyWeight:(NSUInteger)weight age:(NSUInteger)age;

@end
