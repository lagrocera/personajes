//
//  ViewController.h
//  Personajes
//
//  Created by S209e19 on 28/08/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *age;
@property (weak, nonatomic) IBOutlet UITextField *weight;
- (IBAction)chekit:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *response;

@end

